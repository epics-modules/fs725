# -----------------------------------------------------------------------------
# EPICS siteMods
# -----------------------------------------------------------------------------
require stream, 2.8.10
# -----------------------------------------------------------------------------
# EPICS siteApps
# -----------------------------------------------------------------------------
require fs725, master

# -----------------------------------------------------------------------------
# Utgard-Lab - enrivonment params
# -----------------------------------------------------------------------------

## Unique IOC instance name
# Used for IOC stats and autosave status
epicsEnvSet("IOCINST", "SR-CS{IOC:TICKTOCK}")
epicsEnvSet("DEV", "SR-TS{1HZ:PLL}")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(fs725_DIR)db")

## Load record instances
dbLoadRecords("asynRecord.db","P=${DEV},R=Port-ASYN,PORT=aport,ADDR=-1,OMAX=40,IMAX=40")

# Using terminal server
#  Configure in raw socket mode (not reverse telnet)
drvAsynIPPortConfigure("aport","172.30.242.31:4001")

# Using a local serial port
#drvAsynSerialPortConfigure("sr725","/dev/ttyS0",0,0,0)
#asynSetOption("aport", -1, "baud", "9600")
#asynSetOption("aport", -1, "bits", "8")
#asynSetOption("aport", -1, "parity", "none")
#asynSetOption("aport", -1, "stop", "1")
#asynSetOption("aport", -1, "clocal", "Y")
#asynSetOption("aport", -1, "crtscts", "N")

# always needed
asynOctetSetInputEos("aport", -1, "\r")
asynOctetSetOutputEos("aport", -1, "\r\r")

# For debugging.  Produces lots of noise
#asynSetTraceMask("aport",0,0xf)
#asynSetTraceIOMask("aport",0,0x1)

## Load record instances
dbLoadRecords("sr725.db","P=${DEV},PORT=aport")

iocInit()


