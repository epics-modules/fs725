#!/usr/bin/env iocsh.bash

require(fs725)
require(stream)

## Unique IOC instance name, used for IOC stats and autosave status
#epicsEnvSet("IOCNAME", "labs-dev:time-fs725-01")
epicsEnvSet("P", "LABS-amor:time-fs725-01")
epicsEnvSet("IPADDR", "172.30.244.228")
epicsEnvSet("PORT", "4001")


iocshLoad("$(fs725_DIR)/fs725.iocsh", "P=$(P), IPADDR=$(IPADDR), PORT=$(PORT)")

#iocshLoad("$(essioc_DIR)/essioc.iocsh")
#
iocInit()

#EOF
