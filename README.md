# fs725

European Spallation Source ERIC Site-specific EPICS module: fs725

Additonal information:
* [Documentation](https://confluence.esss.lu.se/display/IS/FS725+Rubidium+Frequency+Standard)
* [Release notes](RELEASE.md)
* [Requirements](https://gitlab.esss.lu.se/nice/staging-recipes/fs725-recipe/-/blob/master/recipe/meta.yaml#L18)
* [Building and Testing](https://confluence.esss.lu.se/display/IS/1.+Development+locally+with+e3+and+Conda#id-1.Developmentlocallywithe3andConda-BuildanEPICSmoduleandtestitlocally)
